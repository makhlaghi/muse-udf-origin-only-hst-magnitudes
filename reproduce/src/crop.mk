# Cutout the regions from the images.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Cutout the desired regions
# --------------------------
#
# The depth of the XDF region and the UDF region vary therefore, to
# have uniform noise over each region, we are separating the deep XDF
# field from the rest of the image.
r15seg = $(segdir)/r15seg.fits.gz
udfacs  = $(foreach f, $(xdfacsf),  $(sdepth)/udf_$(f).fits)
xdfwfc3ir = $(foreach f, $(xdfwfc3irf), $(sdepth)/xdf_$(f).fits)
udfwfc3ir = $(foreach f, $(xdfwfc3irf), $(sdepth)/udf_$(f).fits)
udfwfc3uv = $(foreach f, $(udfwfc3uvf), $(sdepth)/udf_$(f).fits)
grdwfc3ir = $(foreach f, $(xdfwfc3irf), $(sdepth)/grd_$(f).fits)

$(r15seg): $(r15segraw) $(iconf)/vertices-udf.mk .gnuastro/astimgcrop.conf    \
	   | $(segdir)
	astimgcrop $(udfpolygon) $< -o$@

$(udfacs): $(sdepth)/udf_%.fits: $(XDF)/$(xdfacspre)%_v1_sci.fits             \
	   $(iconf)/vertices-udf.mk .gnuastro/astimgcrop.conf | $(sdepth)
	astimgcrop $(udfpolygon) $< -o$@

$(udfwfc3uv): $(sdepth)/udf_%.fits: $(UVUDF)/$(uvudfpre)%_v2.0_drz.fits       \
	      $(iconf)/vertices-udf.mk .gnuastro/astimgcrop.conf | $(sdepth)
	astimgcrop $(udfpolygon) --hstartwcs=1 --hendwcs=40 $< -o$@

$(udfwfc3ir): $(sdepth)/udf_%.fits: $(XDF)/$(xdfwfc3irpre)%_v1_sci.fits       \
	      $(iconf)/vertices-lxdf.mk $(iconf)/vertices-udf.mk              \
	      .gnuastro/astimgcrop.conf | $(sdepth)
	astimgcrop $(udfpolygon) $< -o$(sdepth)/udf_$*_t.fits
	astimgcrop $(lxdfpolygon) $(sdepth)/udf_$*_t.fits --outpolygon        \
	           --keepblankcenter -o$@
	rm $(sdepth)/udf_$*_t.fits

$(xdfwfc3ir): $(sdepth)/xdf_%.fits: $(XDF)/$(xdfwfc3irpre)%_v1_sci.fits       \
	      $(iconf)/vertices-xdf.mk $(iconf)/vertices-udf.mk               \
	      .gnuastro/astimgcrop.conf | $(sdepth)
	astimgcrop $(xdfpolygon) $< -o$(sdepth)/xdf_$*_t.fits
	astimgcrop $(udfpolygon) $(sdepth)/xdf_$*_t.fits -o$@
	rm $(sdepth)/xdf_$*_t.fits

$(grdwfc3ir): $(sdepth)/grd_%.fits: $(XDF)/$(xdfwfc3irpre)%_v1_sci.fits       \
	      $(iconf)/vertices-udf.mk .gnuastro/astimgcrop.conf | $(sdepth)
	astimgcrop $(udfpolygon) $< -o$@





# Write info in LaTeX
# -------------------
$(mtexdir)/preps.tex: $(cat) $(iconf)/vertices-*.mk | $(mtexdir)

	n=$$(wc -l $(cat) | awk '{print $$1}');                              \
	echo "\\newcommand{\\numnotinraf}{$$n}"              > $@

	echo "\\newcommand{\\udfraa}{$(udfraa)}"            >> $@
	echo "\\newcommand{\\udfdeca}{$(udfdeca)}"          >> $@
	echo "\\newcommand{\\udfrab}{$(udfrab)}"            >> $@
	echo "\\newcommand{\\udfdecb}{$(udfdecb)}"          >> $@
	echo "\\newcommand{\\udfrac}{$(udfrac)}"            >> $@
	echo "\\newcommand{\\udfdecc}{$(udfdecc)}"          >> $@
	echo "\\newcommand{\\udfrad}{$(udfrad)}"            >> $@
	echo "\\newcommand{\\udfdecd}{$(udfdecd)}"          >> $@
	echo "\\newcommand{\\xdfraa}{$(xdfraa)}"            >> $@
	echo "\\newcommand{\\xdfdeca}{$(xdfdeca)}"          >> $@
	echo "\\newcommand{\\xdfrab}{$(xdfrab)}"            >> $@
	echo "\\newcommand{\\xdfdecb}{$(xdfdecb)}"          >> $@
	echo "\\newcommand{\\xdfrac}{$(xdfrac)}"            >> $@
	echo "\\newcommand{\\xdfdecc}{$(xdfdecc)}"          >> $@
	echo "\\newcommand{\\xdfrad}{$(lxdfrad)}"           >> $@
	echo "\\newcommand{\\xdfdecd}{$(lxdfdecd)}"         >> $@
	echo "\\newcommand{\\lxdfraa}{$(lxdfraa)}"          >> $@
	echo "\\newcommand{\\lxdfdeca}{$(lxdfdeca)}"        >> $@
	echo "\\newcommand{\\lxdfrab}{$(lxdfrab)}"          >> $@
	echo "\\newcommand{\\lxdfdecb}{$(lxdfdecb)}"        >> $@
	echo "\\newcommand{\\lxdfrac}{$(lxdfrac)}"          >> $@
	echo "\\newcommand{\\lxdfdecc}{$(lxdfdecc)}"        >> $@
	echo "\\newcommand{\\lxdfrad}{$(lxdfrad)}"          >> $@
	echo "\\newcommand{\\lxdfdecd}{$(lxdfdecd)}"        >> $@
